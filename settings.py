import math


# SCREEN SETTINGS
WIDTH = 1910
HEIGHT = 950
HALF_WIDTH = WIDTH // 2
HALF_HEIGHT = HEIGHT // 2
FPS = 80
TILE = 100
FPS_POS = (WIDTH - 65, 5)

# COLORS    
WHITE = (255, 255, 255)
BLACK = (0, 0, 0)
RED = (220, 0, 0)
GREEN = (0, 220, 0)
BLUE = (0, 0, 220)
DARKGRAY = (110, 110, 110)
PURPLE = (120, 0, 120)

# PLAYER SETTINGS
player_pos = (HALF_WIDTH, HALF_HEIGHT)
player_speed = 2
player_angle = 0

# RAYS
FOV = math.pi / 3              # область видимости
HALF_FOV = FOV / 2             # крайние 2 луча
NUM_RAYS = 12                  # количество лучей в диапозоне видимости
MAX_DEPTH = 800                # максимальное расстояние(дальность прорисовки)
DELTA_ANGLE = FOV / NUM_RAYS   # угол между лучами
DIST = NUM_RAYS / (2 * math.tan(HALF_FOV))
PROJ_COEFF = 3 * DIST * TILE
SCALE = WIDTH // NUM_RAYS
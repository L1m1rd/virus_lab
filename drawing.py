import pygame
from settings import *
from rays import ray


class Drawing:
    def __init__(self, sc):
        self.sc = sc
        self.font = pygame.font.SysFont("Arial", 36, bold= True)

    def fps(self, clock):
        display_fps = str(int(clock.get_fps()))
        render = self.font.render(display_fps, 0, GREEN)
        self.sc.blit(render, FPS_POS)
